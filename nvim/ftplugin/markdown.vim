set expandtab
set smarttab
set tw=79

" additional coc plugins for markdown
let markdown_coc = ['coc-markdownlint', 'coc-markdown-preview-enhanced', 'coc-markmap']
call extend(g:coc_global_extensions, markdown_coc)

" special key mappings
nnoremap <C-p> :CocCommand markdown-preview-enhanced.openPreview<CR>
