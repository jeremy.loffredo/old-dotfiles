" define whitespace

if has ("patch-7.4.710")
	set listchars=eol:$,tab:>-,trail:·,space:·,extends:>,precedes:<
else
	set listchars=eol:$,tab:>-,trail:.,extends:>,precedes:<
endif

" show all whitespace characters
set list
