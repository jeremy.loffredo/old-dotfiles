" custom controls

" map jj in visual mode to esc
inoremap jj <ESC>

" toggle paste option
function TogglePaste()
	if (&paste == 1)
		set nopaste
	else
		set paste
	endif
endfunction

" map toggle paste
nnoremap <C-v> :call TogglePaste()<CR>

" open file in text
nnoremap gf :vert winc f<cr>

" copies filepath to clipboard
nnoremap <silent> yf :let @+=expand('%:p')<CR>
" copies pwd to clipboard
nnoremap <silent> yd :let @+=expand('%:p:h')<CR>

" use <CR> to confirm completion
"inoremap <expr> <cr> coc#pum#visible() ? coc#pum#confirm() : "\<CR>"

" NERDTree
map <F2> :NERDTreeToggle<CR>
map <C-D> :NERDTreeToggle<CR>

" TagList
map <F3> :TlistToggle<CR>
map <C-L> :TlistToggle<CR>

" Git Fugitive
map <C-s> :Git blame<CR>
