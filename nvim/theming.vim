" theming config

" set color palette
set t_Co=256

set background=dark

if (has("termguicolors"))
	set termguicolors
endif

colorscheme dracula
"colorscheme vividchalk 

" syntax highlighting on
syntax enable

" highlight search terms
hi Search ctermbg=30 ctermfg=None

let g:webdevicons_enable_nerdtree = 1
