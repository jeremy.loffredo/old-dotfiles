" set encoding for the vim scripts
scriptencoding utf-8

" set how vim represents characters internally
set encoding=utf-8

filetype plugin on

" define whitespace
runtime whitespace.vim

" define tabs
runtime spacing.vim

" plugins
runtime plugins.vim

" set theme
runtime theming.vim

" controls
runtime controls.vim

" line numbers
set relativenumber
set number

" config for snipmate
runtime snipmate.vim
