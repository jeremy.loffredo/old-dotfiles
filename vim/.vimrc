scriptencoding utf-8
set encoding=utf-8
if has ("patch-7.4.710")
	set listchars=eol:$,tab:>-,trail:·,space:·,extends:>,precedes:<
else
	set listchars=eol:$,tab:>-,trail:.,extends:>,precedes:<
endif

set t_Co=256
set tabstop=3
set shiftwidth=3
set softtabstop=3
set autoindent
set smartindent
set smarttab
filetype plugin on

set background=dark
set list
" color candy
color vividchalk
syntax on
hi Search ctermbg=30 ctermfg=None
" color distinguished
" color jellybeans

map <F2> <ESC>:NERDTreeToggle<CR>
map <F3> <ESC>:TlistToggle<CR>
map <F4> <ESC>:marks<CR>
map <F5> <ESC>:qa<CR>
map <F6> <ESC>:wqa<CR>
map <F7> <ESC>:qa!<CR>
inoremap jj <ESC>

function! NumberToggle()
	if(&relativenumber == 1)
      set number
	else
		set relativenumber
	endif
endfunc

nnoremap <C-n> :call NumberToggle()<cr>
set relativenumber
set number

let g:NERDTreeGlyphReadOnly = "RO"
call plug#begin()
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fugitive'
call plug#end()
